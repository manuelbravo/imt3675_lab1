package com.example.manuelbravo.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * Main activity of this project.
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener{

    static final String REQUEST = "main.activity.request";
    static final String RESPONSE = "main.activity.response";
    static final int REQUEST_CODE = 1001;

    TextView textMessage;
    private EditText editTextT1;
    private Button btnB1;
    private Spinner spinnerL1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.editTextT1=(EditText)findViewById(R.id.editTextT1);
        this.btnB1=(Button)findViewById(R.id.btnB1);

        btnB1.setOnClickListener(this);
        spinnerL1=findViewById(R.id.spinnerL1);
        spinnerL1.setOnItemSelectedListener(this);
       // this.textMessage = (TextView) findViewById(R.id.text_name);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int option = prefs.getInt("listValue",0);
        spinnerL1.setSelection(option);

    }


    /**
     * Launches the second activity.
     * @param v button that was pressed
     */
    public void launchSecondActivity(final View v) {
        final Intent i = new Intent(this, SecondActivity.class);

        // We dynamically get approriate string from res/values/strings.xml
        i.putExtra(RESPONSE, editTextT1.getText().toString());
        startActivity(i);
        //startActivityForResult(i, REQUEST_CODE);
        // One could use Android predefined transitions, but these are somewhat limited:
         overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        // We will use our own ones:
        //overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }




    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btnB1:
            launchSecondActivity(v);
            break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("listValue", i);
        editor.apply();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
