package com.example.manuelbravo.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by manuelbravo on 01.02.2018.
 */

public class SecondActivity extends AppCompatActivity  implements View.OnClickListener{

    static final String REQUEST = "second.activity.request";
    static final String RESPONSE = "second.activity.response";
    static final int REQUEST_CODE = 1001;

    private TextView textViewT2;
    private TextView textViewT3;
    private Button btnB2;
    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        this.textViewT2 = (TextView) findViewById(R.id.textViewT2);
        this.textViewT3 = (TextView) findViewById(R.id.textViewT3);
        this.btnB2=(Button)findViewById(R.id.btnB2);

        btnB2.setOnClickListener(this);

        // Initiate the request prompt, passed to us from the first activity
        final Intent i = getIntent();
        this.textViewT2.setText("Hello "+i.getStringExtra(MainActivity.RESPONSE));

    }

    public void launchThirdActivity(final View v) {

        final Intent i = new Intent(this, ThirdActivity.class);
        i.putExtra(REQUEST, "Write Something");
        startActivityForResult(i, REQUEST_CODE);
    }

    public void launchMainActivity(final View v) {

        final Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnB2:
                launchThirdActivity(v);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // user has not pressed Back Button

                this.textViewT3.setText("From A3: " + data.getStringExtra(RESPONSE));
            }
        }
    }

}
