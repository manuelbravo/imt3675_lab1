package com.example.manuelbravo.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by manuelbravo on 01.02.2018.
 */

public class ThirdActivity extends AppCompatActivity  implements View.OnClickListener{

    static final String RESPONSE = "third.activity.response";
    private EditText editTextT4;
    private Button btnB3;
    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        this.editTextT4=(EditText)findViewById(R.id.editTextT4);
        this.btnB3=(Button)findViewById(R.id.btnB3);

        btnB3.setOnClickListener(this);
    }

    public void closeActivity(final View v) {

        final Intent intent = new Intent();
        final Bundle bundle = new Bundle();

        bundle.putString(SecondActivity.RESPONSE, this.editTextT4.getText().toString());
        intent.putExtras(bundle);
        this.setResult(RESULT_OK, intent);
        finish();

        // One could use Android predefined transitions, but these are somewhat limited:
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.btnB3:
                closeActivity(v);
                break;
        }
    }
}
